// Definindo os pinos
  const int potPin = A0; // Pino do potenciômetro
  const int weightPin = A1; // Pino do sensor de peso
  const int motorPin = 9; // Pino do motor PWM
// Parâmetros do controlador PID
  float KP = 1.0; // Ganho proporcional
  float CO0 = 0.0; // Saída inicial do controlador
  float COt = 0.0; // Saída do controlador no tempo t
// Variáveis de controle
  float setpoint = 0.0; // Valor desejado do sensor de peso (setpoint)
  float error = 0.0; // Erro entre o setpoint e a leitura atual
  float lastError = 0.0;// Último valor de erro para cálculos diferenciais
  float integral = 0.0; // Termo integral para controlador PID
// Constantes para controle anti-windup
  const float integralLimit = 100.0; // Limite para termo integral
  const float pwmMax = 255.0; // Valor máximo de PWM
// Função para calcular a saída do controlador PID
  float calculatePID() {
// Leitura dos sensores
  float potValue = analogRead(potPin);
  float weightValue = analogRead(weightPin);
// Conversão dos valores para a escala desejada (ajuste conforme necessário)
  float setpoint = map(potValue, 0, 1023, 0, 100); // Exemplo: mapeia potValue para 0-100
  float actualValue = map(weightValue, 0, 1023, 0, 100); // Exemplo: mapeia weightValue
  para 0-100
// Cálculo do erro
  error = setpoint - actualValue;
// Cálculo do termo proporcional
  float proportional = KP * error;
// Cálculo do termo integral com controle anti-windup
  integral += error;
  integral = constrain(integral, -integralLimit, integralLimit);
// Cálculo da saída do controlador
  COt = CO0 + proportional + integral;
// Limitação da saída para evitar valores fora da faixa do PWM
  COt = constrain(COt, 0, pwmMax);
  return COt;

}
void setup() {
  // Configuração dos pinos
  pinMode(motorPin, OUTPUT);
  // Inicialização do controlador
  CO0 = analogRead(weightPin); // Valor inicial da leitura de peso
  setpoint = analogRead(potPin); // Valor inicial do potenciômetro
}
void loop() {
  // Chamada da função para calcular a saída do controlador PID
  float output = calculatePID();
  // Aplicação do valor calculado no motor usando PWM
  analogWrite(motorPin, output);
  // Aguarda um curto intervalo de tempo antes da próxima iteração
  delay(100);
}
